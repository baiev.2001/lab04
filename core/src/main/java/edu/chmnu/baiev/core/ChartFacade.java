package edu.chmnu.baiev.core;

import com.mongodb.client.FindIterable;
import edu.chmnu.baiev.core.dto.Chart;
import edu.chmnu.baiev.core.service.MongoService;
import javafx.event.ActionEvent;
import org.bson.Document;
import org.springframework.stereotype.Service;

@Service
public class ChartFacade {
    private final MongoService mongoService;
    private final GraphDataParser graphDataParser;

    public ChartFacade(MongoService mongoService, GraphDataParser graphDataParser) {
        this.mongoService = mongoService;
        this.graphDataParser = graphDataParser;
    }

    public Chart loadChartData(ActionEvent event) {
        FindIterable<Document> data = mongoService.getData().limit(2);
        Document first = data.first();
        Chart chart = graphDataParser.parse(first.toJson());
//        this.setData(chart.getColumns());
//        for (Document document : data) {
//            Chart chart = graphDataParser.parse(document.toJson());
//            this.setData(chart.getColumns());
//        }
        return chart;
    }
}
