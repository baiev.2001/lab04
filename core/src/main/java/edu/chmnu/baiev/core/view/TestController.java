package edu.chmnu.baiev.core.view;

import com.mongodb.client.FindIterable;
import edu.chmnu.baiev.core.service.MongoService;
import javafx.scene.input.MouseEvent;
import net.rgielen.fxweaver.core.FxmlView;
import org.bson.Document;
import org.springframework.stereotype.Component;

@FxmlView
@Component
public class TestController {

    private final MongoService mongoService;

    public TestController(MongoService mongoService) {
        this.mongoService = mongoService;
    }


    public void doAction(MouseEvent mouseEvent) {
        FindIterable<Document> data = mongoService.getData();
        for (Document document : data) {
            System.out.println(document.toString());
        }
    }
}