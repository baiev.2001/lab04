package edu.chmnu.baiev.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.chmnu.baiev.core.dto.Chart;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Service
public class GraphDataParser {
    private final ObjectMapper objectMapper;
    public GraphDataParser() {
        this.objectMapper = new ObjectMapper();
    }

    public Chart parse(File file) {
        try {
            String json = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
            return objectMapper.readValue(json, Chart.class);
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public Chart parse(String json) {
        try {
            return objectMapper.readValue(json, Chart.class);
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

}
