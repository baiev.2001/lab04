package edu.chmnu.baiev.core.service;

import edu.chmnu.baiev.core.dto.Column;
import edu.chmnu.baiev.core.dto.Type;

import java.util.Collection;
import java.util.stream.Collectors;

public class ColumnRepository {
    private Collection<Column> columns;

    public ColumnRepository(Collection<Column> columns) {
        this.columns = columns;
    }

    public Column getXAxis() {
        return this.columns.stream()
                .filter(item -> item.getType().equals(Type.X))
                .findFirst()
                .orElseThrow();
    }

    public Collection<Column> getFunctions() {
        return this.columns.stream()
                .filter(item -> item.getType().equals(Type.LINE))
                .collect(Collectors.toList());
    }
}
