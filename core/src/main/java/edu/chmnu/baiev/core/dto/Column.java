package edu.chmnu.baiev.core.dto;

import javafx.scene.paint.Color;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class Column {
    private String id;
    private String name;
    private List<Long> values;
    private Type type;
    private Color color;
}
