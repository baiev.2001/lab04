package edu.chmnu.baiev.core.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import edu.chmnu.baiev.core.serializer.ChartDeserializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonDeserialize(using = ChartDeserializer.class)
public class Chart {
    private List<Column> columns;
//    private List<Type> types;
//    private List<Color> colors;
//    private List<String> names;
}
