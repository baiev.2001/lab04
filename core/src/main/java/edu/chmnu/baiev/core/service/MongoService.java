package edu.chmnu.baiev.core.service;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.springframework.stereotype.Service;

@Service
public class MongoService {

    private final MongoClient mongoClient;

    public MongoService(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }

    public com.mongodb.client.FindIterable<Document> getData() {
        MongoDatabase test = mongoClient.getDatabase("charts-db");
        MongoCollection<Document> testCollection = test.getCollection("chart_data");
        return testCollection.find();
    }
}
