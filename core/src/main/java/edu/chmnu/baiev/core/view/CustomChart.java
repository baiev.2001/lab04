package edu.chmnu.baiev.core.view;

import edu.chmnu.baiev.core.dto.Column;
import edu.chmnu.baiev.core.service.ColumnRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.HBox;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@FxmlView
@Component
public class CustomChart {

    private static final SimpleDateFormat CHART_DATE_FORMAT = new SimpleDateFormat("MMM-dd");

    @FXML
    private CategoryAxis xAxis;
    @FXML
    private LineChart<String, Long> lineChart;
    @FXML
    private HBox chartButtonsPane;
    private ObservableList<String> xCategories = FXCollections.observableArrayList();

    @FXML
    private void initialize() {
        xAxis.setCategories(xCategories);
    }

    protected final void setData(List<Column> columns) {
//________________Set X Axis______________________________________________
        ColumnRepository columnRepository = new ColumnRepository(columns);
        Column xColumn = columnRepository.getXAxis();
        for (Long time : xColumn.getValues()) {
            Date date = new Date(time);
            String formattedDate = CHART_DATE_FORMAT.format(date);
            if (!xCategories.contains(formattedDate)) {
                xCategories.add(formattedDate);
            }
        }
//________________________________________________________________________
        /*________________Set Functions to the chart______________________________
         * Params:
         *   + Collection<Column> lines - lines to set
         *   + xCategories - id of item in xColumn == x value of function[i]
         *   + lineChart - chart to add new data.
         * */
        Collection<Column> lines = columnRepository.getFunctions();
        lines.forEach(
                line -> {
                    XYChart.Series<String, Long> series = new XYChart.Series<>();
//                    Button button = new Button();
//                    button.setAlignment(Pos.CENTER_LEFT);
//                    button.setText(line.getName());
//                    button.setStyle("-fx-background-color:" + Utils.toHexColor(line.getColor()));
//                    button.setOnMouseClicked(mouseEvent -> {
//                        button.setDisable(true);
//                        try {
//                            if (!lineChart.getData().remove(series)) {
//                                lineChart.getData().add(series);
//                            }
//                        } finally {
//                            button.setDisable(false);
//                        }
//                    });
//                    chartButtonsPane.getChildren().add(button);
                    List<Long> values = line.getValues();
                    for (int i = 0; i < values.size(); ++i) {
                        series.getData().add(new XYChart.Data<>(xCategories.get(i), values.get(i)));
                    }
                    lineChart.getData().add(series);

//                    Node lineNode = series.getNode().lookup(".chart-series-line");
//                    lineNode.setStyle("-fx-stroke: "+ Utils.toHexColor(line.getColor()));
                }
        );
//___________________________________________________________
    }

    XYChart.Series<String, Long> getData(int index) {
        return this.lineChart.getData().get(index);
    }
}
