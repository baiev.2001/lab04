package edu.chmnu.baiev.core;

import edu.chmnu.baiev.core.view.FXApplication;
import javafx.application.Application;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MainApp {

    public static void main(String[] args) {
        Application.launch(FXApplication.class, args);
    }
}
