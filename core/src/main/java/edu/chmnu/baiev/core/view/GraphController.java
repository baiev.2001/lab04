package edu.chmnu.baiev.core.view;

import edu.chmnu.baiev.core.ChartFacade;
import edu.chmnu.baiev.core.dto.Chart;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TextField;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@FxmlView
@Component
public class GraphController {
    private final ChartFacade chartFacade;
    @FXML
    public TextField from;
    @FXML
    public TextField to;

    @FXML
    private final CustomChart lineChart;

    @Autowired
    public GraphController(ChartFacade chartFacade,
                           CustomChart lineChart) {
        this.chartFacade = chartFacade;
        this.lineChart = lineChart;
    }

    @FXML
    private void findDecline(ActionEvent event) {
        String text = this.from.getText();
        String text1 = this.to.getText();
        Integer from = Integer.valueOf(text);
        Integer to = Integer.valueOf(text1);

        XYChart.Series<String, Long> function = lineChart.getData(1);
        ObservableList<XYChart.Data<String, Long>> functionDataList = function.getData();
        Map<Long, Long> pairs = new HashMap<>();
        long startValue = functionDataList.get(from).getYValue();
        for (int i = from; i < to; ++i) {
            XYChart.Data<String, Long> functionData = functionDataList.get(i);
            Long functionValue = functionData.getYValue();
            Long nextFunctionValue = functionDataList.get(i + 1).getYValue();
            if (functionValue <= nextFunctionValue) {
                continue;
            }
            if (functionValue > nextFunctionValue) {
                pairs.putIfAbsent(startValue, functionValue);
                startValue = nextFunctionValue;
                continue;
            }
            startValue = functionValue;
        }
        System.out.println(pairs);
    }

    @FXML
    private void loadChartData(ActionEvent event) {
        Chart chart = chartFacade.loadChartData(event);
        lineChart.setData(chart.getColumns());
    }
}
